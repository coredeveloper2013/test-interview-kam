<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_system_admin = Role::where('name', 'system_admin')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_manager = Role::where('name', 'manager')->first();
        $role_customer = Role::where('name', 'customer')->first();
        
        $system_admin = new User();
        $system_admin->name = 'System admin Name';
        $system_admin->email = 'system_admin@example.com';
        $system_admin->password = bcrypt('secret');
        $system_admin->save();
        $system_admin->roles()->attach($role_system_admin);
        
        $admin = new User();
        $admin->name = 'Admin Name';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('secret');
        $admin->save();
        $admin->roles()->attach($role_admin);
        
        $manager = new User();
        $manager->name = 'Manager Name';
        $manager->email = 'manager@example.com';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($role_manager);
        
        $customer = new User();
        $customer->name = 'Customer Name';
        $customer->email = 'customer@example.com';
        $customer->password = bcrypt('secret');
        $customer->save();
        $customer->roles()->attach($role_customer);
    }
}
