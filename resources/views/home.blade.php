@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">System admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                    <h3>Create Admin</h3>
                    @if ( count($errors) > 0 )
					    @foreach ( $errors->all() as $error ) 
						    <p class="alert alert-danger">{{ $error }}</p>
					    @endforeach
                    @endif
                    
                    <form action="{{ route('create-admin') }}" method="post">
                        {{ csrf_field() }}
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ old('name') }}">
                        <br>
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{ old('email') }}">
                        <br>
                        <label for="password">Password</label>
                        <input type="password" name="password" value="">
                        <br>
                        <input type="submit" value="Create">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
